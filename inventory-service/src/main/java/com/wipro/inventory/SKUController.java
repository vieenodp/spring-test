package com.wipro.inventory;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import java.util.Collections;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;


@RestController 

public class SKUController {
	
	static AtomicInteger ID = new AtomicInteger();
	List<SKU> skus = new ArrayList<>();
	
	@GetMapping ("/skus/")
	public List<SKU> getSKUs() {
		return Collections.emptyList();
	}
	
	@PostMapping("/skus/")
	public SKU addSKU(@RequestBody SKU newSku) {
		newSku.setId(ID.incrementAndGet());
		skus.add(newSku);
		return newSku;
	}
	

}
